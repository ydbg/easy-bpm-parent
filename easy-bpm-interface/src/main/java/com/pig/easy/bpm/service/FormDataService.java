package com.pig.easy.bpm.service;

import com.pig.easy.bpm.utils.Result;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
public interface FormDataService {

    Result<Integer> batchSaveOrUpdateFormData(Long applyId,Long taskId,String tenantId,String formKey, Map<String,Object> dataMap);

    Result<Map<String,Object>> getFormDataByApplyId(Long applyId);

    Result<Map<String,Object>> getFormDataByProcInstId(String procInstId);

}
