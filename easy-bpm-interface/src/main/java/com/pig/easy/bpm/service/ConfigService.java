package com.pig.easy.bpm.service;


import com.pig.easy.bpm.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
public interface ConfigService {

    Result<Object> getConfigValue(String tenantId, String configKey);

}
