package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.utils.Result;
/**
 * <p>
 * 变量表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
public interface VariableDictService {

        Result<PageInfo<VariableDictDTO>> getListByCondition(VariableDictQueryDTO param);

        Result<Integer> insertVariableDict(VariableDictSaveOrUpdateDTO param);

        Result<Integer> updateVariableDict(VariableDictSaveOrUpdateDTO param);

        Result<Integer> deleteVariableDict(VariableDictSaveOrUpdateDTO param);

 }
