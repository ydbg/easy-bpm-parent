package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.utils.Result;
/**
 * <p>
 * 通知白名单 服务类
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
public interface MessageWhiteListService {

        Result<PageInfo<MessageWhiteListDTO>> getListByCondition(MessageWhiteListQueryDTO param);

        Result<Integer> insertMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param);

        Result<Integer> updateMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param);

        Result<Integer> deleteMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param);

 }
