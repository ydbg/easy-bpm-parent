package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 18:58
 */
@Data
@ToString
public class DictItemDTO extends BaseResponseDTO {

    private static final long serialVersionUID = -123816316865188022L;

    private Long itemId;

    private Long dictId;

    private String itemValue;

    private String itemText;

    private String tenantId;

    private Integer sort;

    private String remark;

    private Integer validState;

    private Long operatorId;

    private String operatorName;
}
