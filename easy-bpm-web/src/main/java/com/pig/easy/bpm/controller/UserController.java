package com.pig.easy.bpm.controller;


import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.annotations.Login;
import com.pig.easy.bpm.dto.request.AddUserDTO;
import com.pig.easy.bpm.dto.request.UserQueryDTO;
import com.pig.easy.bpm.dto.request.UserUpdateDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.UserService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.LoginVO;
import com.pig.easy.bpm.vo.request.OrganUserQueryVO;
import com.pig.easy.bpm.vo.request.UserQueryVO;
import com.pig.easy.bpm.vo.request.UserSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-14
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户管理", value = "用户管理")
public class UserController extends BaseController {

    @DubboReference
    UserService userService;

    @ApiOperation(value = "用户登录", notes = "用户登录")
    @RequestMapping("/login")
    @Login(false)
    public JsonResult login(@RequestBody @Valid LoginVO loginVO) {

        System.out.println("@##################loginVO = " + loginVO);
        Result<UserInfoDTO> result = userService.login(loginVO.getUsername(), loginVO.getPassword());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增用户", notes = "新增用户",  produces = "application/json")
    @PostMapping("/addUser")
    public JsonResult addUser(@Valid UserSaveOrUpdateVO userSaveOrUpdateVO) {

        if(userSaveOrUpdateVO == null){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        AddUserDTO addUserDTO = switchToDTO(userSaveOrUpdateVO,AddUserDTO.class);
        Result<UserInfoDTO> result = userService.addUser(addUserDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改用户", notes = "修改用户",  produces = "application/json")
    @PostMapping("/updateUser")
    public JsonResult updateUser(@Valid UserSaveOrUpdateVO userSaveOrUpdateVO) {

        if(userSaveOrUpdateVO == null){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        UserUpdateDTO updateDTO = switchToDTO(userSaveOrUpdateVO,UserUpdateDTO.class);
        Result<Integer> result = userService.updateUser(updateDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }


    @ApiOperation(value = "退出登录", notes = "新增用户",  produces = "application/json")
    @PostMapping("/logout")
    public JsonResult logout() {

        Result<Boolean> result = userService.logout(currentUserInfo().getUserId(),currentUserInfo().getAccessToken());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取用户详情", notes = "获取用户详情",  produces = "application/json")
    @PostMapping("/getUserInfo/{username}")
    public JsonResult getUserInfo(@ApiParam(required = true, name = "用户名称", value = "username", example = "pig") @PathVariable("username") String username) {

        Result<UserInfoDTO> result = userService.getUserInfo(username);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取机构人员树", notes = "获取机构人员树",  produces = "application/json")
    @PostMapping("/getOrganUserTree")
    public JsonResult getOrganUserTree(@RequestBody @Valid OrganUserQueryVO organUserQueryVO) {

        Result<List<TreeDTO>> result = userService.getOrganUserTree(organUserQueryVO.getTenantId(), organUserQueryVO.getParentId());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取人员列表", notes = "获取人员列表",  produces = "application/json")
    @PostMapping("/getUserList")
    public JsonResult getUserList(@RequestBody @Valid UserQueryVO userQueryVO) {

        if (userQueryVO == null) {
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        UserQueryDTO userQueryDTO = BeanUtils.switchToDTO(userQueryVO,UserQueryDTO.class);
        Result<PageInfo<UserInfoDTO>> result = userService.getListByCondition(userQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

}

