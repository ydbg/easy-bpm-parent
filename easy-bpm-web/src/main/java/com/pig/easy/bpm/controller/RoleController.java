package com.pig.easy.bpm.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@RestController
@RequestMapping("/roleDO")
public class RoleController extends BaseController {

}

