package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.entity.FormDataDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Mapper
public interface FormDataMapper extends BaseMapper<FormDataDO> {

    int batchSave(@Param("list") List<FormDataDO> updateFormDataList);

    int batchUpdate(@Param("list") List<FormDataDO> updateFormDataList);
}
