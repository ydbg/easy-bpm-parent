package com.pig.easy.bpm;

import com.alibaba.fastjson.JSON;
import com.pig.easy.bpm.utils.CommonUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class EasyBpmProviderApplicationTests {

    @Test
    void contextLoads() {
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("nacos"));
        String encode = new BCryptPasswordEncoder().encode("123456");
        System.out.println(encode);
        System.out.println("encode = "+ new BCryptPasswordEncoder().matches("123456",encode));
        System.out.println("encode = " + encode);

        System.out.println("encode = " + CommonUtils.formatDate(LocalDateTime.now(),CommonUtils.yyyy_MM_ddHHmmss));;


        List<String> list = new ArrayList<>();

        System.out.println("JSON.toJSONString(list.toString() = " + JSON.toJSONString(list.toString()));

        System.out.println(StringEscapeUtils.unescapeJava(JSON.toJSONString(list)));

    }

}
