package com.pig.easy.bpm.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.dto.request.AddUserDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.service.UserService;
import com.pig.easy.bpm.utils.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    UserService userService;

    @Test
    void addUsers() {

        AddUserDTO addUserDTO = new AddUserDTO();
        addUserDTO.setUserName("admin");
        Result<UserInfoDTO> responseResult = userService.addUser(addUserDTO);
        Assert.isNull(responseResult, "插入失败");
    }

    @Test
    void getOrganUserTree() {


        Result<List<TreeDTO>> responseResult = userService.getOrganUserTree( "pig",  0L);
        Assert.isNull(responseResult, "插入失败");
    }
}