package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.dto.response.MenuTreeDTO;
import com.pig.easy.bpm.service.MenuService;
import com.pig.easy.bpm.utils.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
public class MenuServiceImplTest {

    @Autowired
    MenuService menuService;

    @Test
    void getMenuTree() {

        Result<List<MenuTreeDTO>> result = menuService.getMenuTree("pig", null);

        System.out.println("result = " + result);
        Assert.isNull(result);
    }
}